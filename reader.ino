#include "RF24.h"
#include "printf.h"
#include <Time.h>
#include "EEPROMAnything.h"

//Gere o enviar mensagem ao hub a cada 60seg
int running = 0;
boolean mutexRunning = false;

//PIN onde os IR estão ligados
int IRa=0;
int IRb=1;

//Average de valores
int avgA[5] = {0,0,0,0,0};
int avgB[5] = {0,0,0,0,0};
int count=0;

//Sentido do veículo
bool flagLeft=false;
bool flagRight=false;
bool flagMiddle=false;

//limites de distâncias
#define MAX_DISTANCE 40
#define MIN_DISTANCE 10

//transmiter data
bool done = false;
RF24 radio(9,10);

// Estrutura de configuração para escrever na ROM, mantendo pressistência de dados em reboot
struct config_t
{
  byte pipe; 
  byte mode;
  byte config_set;
  int vehicleCount;
  int transmitterId;
} eeprom_config;

// Escreve valores default na ROM
void set_EEPROM_Default() {
    eeprom_config.pipe = 1;
    eeprom_config.config_set=1;
    eeprom_config.vehicleCount = 0;
    eeprom_config.transmitterId = 99;
}

void read_EEPROM_Settings() {
  //INSERIR || NO IF PARA QUANDO É PRESSIONADO O BOTAO DE RESET
  
  // read the current config
  EEPROM_readAnything(0, eeprom_config);
  
  if (eeprom_config.config_set < 1) {
    // set default values
    printf("Setting values...\r\n");
    set_EEPROM_Default();
    
    // write the config to eeprom
    EEPROM_writeAnything(0, eeprom_config);
  } 
}

void print_EEPROM_Settings() {
  Serial.print("pipe: ");
  Serial.println((long)get_pipe(eeprom_config.pipe));
  Serial.print("config set: ");
  Serial.println(eeprom_config.config_set);
  Serial.print("count: ");
  Serial.println(eeprom_config.vehicleCount);
  Serial.print("transmitterId: ");
  Serial.println(eeprom_config.transmitterId);
}

uint64_t get_pipe(char pipe){
  switch (pipe) {
    case 1:
      return 0xABCDABCD71LL;
      break;
    case 2:
      return 0xF0F0F0F0D2LL;
      break;
    case 3:
      return 0xF0F0F0F0C3LL;
      break;
    case 4:
      return 0xF0F0F0F0B4LL;
      break;
    case 5:
      return 0xF0F0F0F0A5LL;
      break;
    default: 
      return 0xF0F0F0F096LL;
      break;
  }
}

void open_radio_Settings(){
  radio.begin();
  radio.setRetries(15,15);
  radio.openWritingPipe(get_pipe(eeprom_config.pipe));
  radio.openReadingPipe(1,get_pipe(eeprom_config.pipe));
  radio.startListening();
}

void setupHub() {
  read_EEPROM_Settings();
  print_EEPROM_Settings();
  open_radio_Settings();
}

void setup(void)
{
  pinMode(IRa,OUTPUT); //declara os pins
  pinMode(IRb,OUTPUT);
  
  Serial.begin(9600);
  printf_begin();
  printf("\n\rReader\n\r");

  delay(200); // some time to settle
  setupHub();
  delay(200); // some time to settle
}

void loop(void)
{
  countVehicles(); //faz a contagem de veículos
  sendTimer(); //faz a comunicação com o hub
  delay(100);
}

void countVehicles() {
  avgA[count]=analogRead(IRa);
  avgB[count]=analogRead(IRb);
  count++;
  if(count==5){
    count=0;
  } //média de 5 contagens a cada 100ms

  int sOne = avgDistance(avgA); //o valor é sempre uma média de 5 contagens
  int sTwo = avgDistance(avgB);
  
  if(!inRange(sOne) && !inRange(sTwo)){ //caso não exista nada em range, fecha todas as flags
    flagRight=false;
    flagLeft=false;
    flagMiddle=false;
  } else if(inRange(sOne) && !inRange(sTwo) && !flagLeft){ //caso a direita esteja em range e a esquerda não, abre a flag direita
    Serial.println("left");
    if(flagRight && flagMiddle){
      flagRight = false;
      flagMiddle = false;
      eeprom_config.vehicleCount--;
      printf("Right to Left - Vehicle Count: %d\n\r",eeprom_config.vehicleCount);
    }
    flagLeft = true;
  } else if(!inRange(sOne) && inRange(sTwo) && !flagRight) {  //caso a esquerda esteja em range e a direita não, abre a flag esquerda
    Serial.println("right");
    if(flagLeft && flagMiddle){
      flagLeft = false;
      flagMiddle = false;
      eeprom_config.vehicleCount++;
      printf("Left To Right - Vehicle Count: %d\n\r",eeprom_config.vehicleCount);
    }
    flagRight = true;
  } else if(inRange(sOne) && inRange(sTwo) && !flagMiddle && (flagRight || flagLeft) && !(flagRight && flagLeft)) { //caso os dois estejam em range, uma flag lateral esteja aberta e não estejam as duas laterais abertas
    Serial.println("Middle");
    flagMiddle = true;
  }
 
}

//gere o timer para enviar ao hub apenas uma mensagem no segundo 2 de cada minuto
void sendTimer() {
  if(second() == 2) {
    mutexRunning = true;
  } else {
    mutexRunning = false;
    running = 0;
    done = false;
  }
  if(mutexRunning && running == 0) {
    running = 1;
    send();
  }
}

//envia a mensagem ao hub
boolean send()
{
    if(running == 1) {
      radio.powerUp();
      if (!done)
      {
        radio.stopListening();
        printf("Now sending ...");
    
        int message = concat(eeprom_config.transmitterId, eeprom_config.vehicleCount);
        printf(" %d ",message);
        bool ok = radio.write(&message, 4);
        
        if (ok) {
          printf("ok...\n\r");
          eeprom_config.vehicleCount=0;
          done = true;
          running = 0;
        } else {
          printf("failed.\n\r");
          done = false;
        }
      }
      radio.powerDown(); 
    }
}

//faz a média da distancia de leitura de um sensor
int avgDistance(int val[]){
  int avgDistance = 0;
  int averaging = 0;
  for (int i = 0; i < 5; i++){
    averaging = averaging + val[i];
  }
  avgDistance = averaging / 5;
  return(distance(avgDistance));
}

//sabe se um valor está dentro do range declarado para ser uma distância válida
bool inRange(int val){
  if(val < MAX_DISTANCE && val > MIN_DISTANCE)
    return true;
  return false;
}

//passa a medida do sensor IR para cm
int distance(int val){
  int i = (6762/(val-9))-4;
  if(i<0)
    return 0;
  if(i>MAX_DISTANCE)
    return 80;
  return i;
}

//concat de ints
int concat(int a, int b)
{
    return a*100 + b;
}
